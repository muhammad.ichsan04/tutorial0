package id.ac.ui.cs.advprog.demo1.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Student {
    private String name;
    private String npm;
    private String address;
}
