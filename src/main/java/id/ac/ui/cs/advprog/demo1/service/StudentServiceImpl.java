package id.ac.ui.cs.advprog.demo1.service;

import id.ac.ui.cs.advprog.demo1.exception.DuplicateStudentNameException;
import id.ac.ui.cs.advprog.demo1.model.Student;
import id.ac.ui.cs.advprog.demo1.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student create(Student student) {
        validateName(student);
        generateNPM(student);
        studentRepository.create(student);
        return student;
    }

    private void validateName(Student student) {
        List<Student> allStudents = findAll();

        for(Student dbStudent : allStudents) {
            if (dbStudent.getName().equals(student.getName())) {
                throw new DuplicateStudentNameException(student.getName());
            }
        }
    }

    private void generateNPM(Student student) {
        StringBuilder stringBuilder = new StringBuilder();

        for (char letter : student.getName().toCharArray()) {
            stringBuilder.append(String.valueOf((int)letter));
        }

        String npm = stringBuilder.toString();
        student.setNpm(npm);
    }

    public List<Student> findAll() {
        Iterator<Student> studentIterator = studentRepository.findAll();
        List<Student> allStudents = new ArrayList<>();
        studentIterator.forEachRemaining(allStudents::add);

        return allStudents;
    }
}
